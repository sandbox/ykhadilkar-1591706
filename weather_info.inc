<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ykhadilkar
 * Date: 5/20/12
 * Time: 4:15 PM
 * To change this template use File | Settings | File Templates.
 */

function weather_info_get_weather($location,$language){
    $requestAddress = sprintf('http://www.google.com/ig/api?weather=%s&h1=%s',url($location),$language);
    try {
        $xml_str = utf8_encode(file_get_contents($requestAddress, 0));
        $weather = new SimplexmlElement($xml_str);
        if (isset($weather->weather->problem_cause)) {
            throw new Exception (t("Can't load %loc", array('%loc' => $location)));
        }
        if(!$weather) {
            throw new Exception ('weather failed');
        }
    } catch (Exception $err) {
        watchdog ('weather_info', 'Cannot get weather for %location: %message',
            array('%location' => $location, '%message' => $err->getMessage()),
            WATCHDOG_ERROR);
        return null;
    }
    return $weather;
}

function weather_info_temp($in_temp, $unit, $unit_system) {
    return sprintf('%s&deg;%s', $in_temp, $unit);
}